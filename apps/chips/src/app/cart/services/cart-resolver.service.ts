import { Injectable } from '@angular/core';
import { CartFacade } from '../+state/cart.facade';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { CartProduct, CartState } from '../+state/cart.reducer';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable()
export class CartResolverService implements Resolve<Observable<CartState>> {
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<CartState> {
    return this.cartFacade.cart$.pipe(first());
  }

  constructor(private cartFacade: CartFacade) {
    cartFacade.loadCart();
  }
}
