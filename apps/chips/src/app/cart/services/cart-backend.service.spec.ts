import { TestBed } from '@angular/core/testing';

import { CartBackendService } from './cart-backend.service';

describe('CartBackendServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CartBackendService = TestBed.get(CartBackendService);
    expect(service).toBeTruthy();
  });
});
