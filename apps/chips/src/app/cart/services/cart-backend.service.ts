import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CartProduct } from '../+state/cart.reducer';

@Injectable({
  providedIn: 'root'
})
export class CartBackendService {
  private _state: BehaviorSubject<CartProduct[]>;
  private _items: CartProduct[];
  constructor() {
    this._items = [];
    this._state = new BehaviorSubject(this._items);
  }

  addItemToCart(item: CartProduct) {
    this._items.push(item);
    this._state.next(this._items);
  }

  loadCart(): Observable<CartProduct[]> {
    return this._state.asObservable();
  }
}
