import { CartLoaded } from './cart.actions';
import { CartState, Entity, initialState, cartReducer } from './cart.reducer';

describe('Cart Reducer', () => {
  const getCartId = it => it['id'];
  let createCart;

  beforeEach(() => {
    createCart = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
  });

  describe('valid Cart actions ', () => {
    it('should return set the list of known Cart', () => {
      const carts = [createCart('PRODUCT-AAA'), createCart('PRODUCT-zzz')];
      const action = new CartLoaded(carts);
      const result: CartState = cartReducer(initialState, action);
      const selId: string = getCartId(result.list[1]);

      expect(result.loaded).toBe(true);
      expect(result.list.length).toBe(2);
      expect(selId).toBe('PRODUCT-zzz');
    });
  });

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;
      const result = cartReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
