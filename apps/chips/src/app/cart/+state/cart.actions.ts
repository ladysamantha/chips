import { Action } from '@ngrx/store';
import { CartProduct } from './cart.reducer';
import { Product } from '../../products/+state/products.reducer';

export enum CartActionTypes {
  LoadCart = '[Cart] Load Cart',
  CartLoaded = '[Cart] Cart Loaded',
  CartLoadError = '[Cart] Cart Load Error',
  AddProductToCart = '[Cart] Add Product To Cart',
  ProductAddedToCart = '[Cart] Product Added To Cart',
  AddProductToCartError = '[Cart] Add Product To Cart Error'
}

export class LoadCart implements Action {
  readonly type = CartActionTypes.LoadCart;
}

export class CartLoadError implements Action {
  readonly type = CartActionTypes.CartLoadError;
  constructor(public payload: any) {}
}

export class CartLoaded implements Action {
  readonly type = CartActionTypes.CartLoaded;
  constructor(public payload: CartProduct[]) {}
}

export class AddProductToCart implements Action {
  readonly type = CartActionTypes.AddProductToCart;
  constructor(public payload: Product) {}
}

export class AddProductToCartError implements Action {
  readonly type = CartActionTypes.AddProductToCartError;
  constructor(public payload: Error) {}
}

export class ProductAddedToCart implements Action {
  readonly type = CartActionTypes.ProductAddedToCart;
  constructor(public payload: CartProduct) {}
}

export type CartAction =
  | LoadCart
  | CartLoaded
  | CartLoadError
  | AddProductToCart
  | ProductAddedToCart
  | AddProductToCartError;

export const fromCartActions = {
  LoadCart,
  CartLoaded,
  CartLoadError,
  AddProductToCart,
  ProductAddedToCart,
  AddProductToCartError
};
