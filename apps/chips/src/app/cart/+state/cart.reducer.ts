import { CartAction, CartActionTypes } from './cart.actions';
import { Product } from '../../products/+state/products.reducer';
import { ifError } from 'assert';

export const CART_FEATURE_KEY = 'cart';

export interface CartProduct extends Product {
  quantity: number;
  isGift: boolean;
}

export interface CartState {
  items: CartProduct[];
  loaded: boolean;
  error?: any;
}

export interface CartPartialState {
  readonly [CART_FEATURE_KEY]: CartState;
}

export const initialState: CartState = {
  items: [],
  loaded: false
};

export function cartReducer(
  state: CartState = initialState,
  action: CartAction
): CartState {
  switch (action.type) {
    case CartActionTypes.CartLoaded: {
      return {
        ...state,
        items: action.payload,
        loaded: true
      };
    }
    case CartActionTypes.ProductAddedToCart: {
      return {
        ...state,
        items: [...state.items, action.payload]
      };
    }
    case CartActionTypes.CartLoadError:
    case CartActionTypes.AddProductToCartError:
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
}
