import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CART_FEATURE_KEY, CartState } from './cart.reducer';

// Lookup the 'Cart' feature state managed by NgRx
const getCartState = createFeatureSelector<CartState>(CART_FEATURE_KEY);

const getLoaded = createSelector(
  getCartState,
  (state: CartState) => state.loaded
);

const getError = createSelector(
  getCartState,
  (state: CartState) => state.error
);

const getCart = createSelector(
  getCartState,
  getLoaded,
  (state: CartState, isLoaded) => {
    return isLoaded ? state : null;
  }
);

export const cartQuery = {
  getLoaded,
  getError,
  getCart
};
