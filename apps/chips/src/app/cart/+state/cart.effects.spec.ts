import { TestBed, async } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';

import { NxModule } from '@nrwl/nx';
import { DataPersistence } from '@nrwl/nx';
import { hot } from '@nrwl/nx/testing';

import { CartEffects } from './cart.effects';
import { LoadCart, CartLoaded } from './cart.actions';

describe('CartEffects', () => {
  let actions: Observable<any>;
  let effects: CartEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NxModule.forRoot(),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([])
      ],
      providers: [
        CartEffects,
        DataPersistence,
        provideMockActions(() => actions)
      ]
    });

    effects = TestBed.get(CartEffects);
  });

  describe('loadCart$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: new LoadCart() });
      expect(effects.loadCart$).toBeObservable(
        hot('-a-|', { a: new CartLoaded([]) })
      );
    });
  });
});
