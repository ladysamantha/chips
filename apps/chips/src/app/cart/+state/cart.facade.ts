import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { CartPartialState } from './cart.reducer';
import { cartQuery } from './cart.selectors';
import { LoadCart, AddProductToCart } from './cart.actions';
import { Product } from '../../products/+state/products.reducer';

@Injectable()
export class CartFacade {
  loaded$ = this.store.pipe(select(cartQuery.getLoaded));
  cart$ = this.store.pipe(select(cartQuery.getCart));

  constructor(private store: Store<CartPartialState>) {}

  loadCart() {
    this.store.dispatch(new LoadCart());
  }

  addProductToCart(product: Product) {
    this.store.dispatch(new AddProductToCart(product));
  }
}
