import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';

import { CartPartialState, CartProduct } from './cart.reducer';
import {
  LoadCart,
  CartLoaded,
  CartLoadError,
  CartActionTypes,
  AddProductToCart,
  ProductAddedToCart,
  AddProductToCartError
} from './cart.actions';
import { CartBackendService } from '../services/cart-backend.service';

@Injectable()
export class CartEffects {
  @Effect() loadCart$ = this.dataPersistence.fetch(CartActionTypes.LoadCart, {
    run: (action: LoadCart, state: CartPartialState) => {
      // Your custom REST 'load' logic goes here. For now just return an empty list...
      return new CartLoaded([]);
    },

    onError: (action: LoadCart, error) => {
      console.error('Error', error);
      return new CartLoadError(error);
    }
  });

  @Effect() addProductToCart$ = this.dataPersistence.pessimisticUpdate(
    CartActionTypes.AddProductToCart,
    {
      run: (action: AddProductToCart, state: CartPartialState) => {
        const item: CartProduct = {
          ...action.payload,
          quantity: 1,
          isGift: false
        };
        this.backend.addItemToCart(item);
        return new ProductAddedToCart(item);
      },
      onError: (action: AddProductToCart, error: any) => {
        console.error('Error adding product to cart', error);
        return new AddProductToCartError(error);
      }
    }
  );

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<CartPartialState>,
    private backend: CartBackendService
  ) {}
}
