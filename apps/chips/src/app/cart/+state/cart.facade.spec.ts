import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { readFirst } from '@nrwl/nx/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/nx';

import { CartEffects } from './cart.effects';
import { CartFacade } from './cart.facade';

import { cartQuery } from './cart.selectors';
import { LoadCart, CartLoaded } from './cart.actions';
import { CartState, Entity, initialState, cartReducer } from './cart.reducer';

interface TestSchema {
  cart: CartState;
}

describe('CartFacade', () => {
  let facade: CartFacade;
  let store: Store<TestSchema>;
  let createCart;

  beforeEach(() => {
    createCart = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
  });

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature('cart', cartReducer, { initialState }),
          EffectsModule.forFeature([CartEffects])
        ],
        providers: [CartFacade]
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule
        ]
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.get(Store);
      facade = TestBed.get(CartFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async done => {
      try {
        let list = await readFirst(facade.allCart$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.loadAll();

        list = await readFirst(facade.allCart$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `CartLoaded` to manually submit list for state management
     */
    it('allCart$ should return the loaded list; and loaded flag == true', async done => {
      try {
        let list = await readFirst(facade.allCart$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        store.dispatch(new CartLoaded([createCart('AAA'), createCart('BBB')]));

        list = await readFirst(facade.allCart$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(2);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });
});
