import { Entity, CartState } from './cart.reducer';
import { cartQuery } from './cart.selectors';

describe('Cart Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getCartId = it => it['id'];

  let storeState;

  beforeEach(() => {
    const createCart = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
    storeState = {
      cart: {
        list: [
          createCart('PRODUCT-AAA'),
          createCart('PRODUCT-BBB'),
          createCart('PRODUCT-CCC')
        ],
        selectedId: 'PRODUCT-BBB',
        error: ERROR_MSG,
        loaded: true
      }
    };
  });

  describe('Cart Selectors', () => {
    it('getAllCart() should return the list of Cart', () => {
      const results = cartQuery.getAllCart(storeState);
      const selId = getCartId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelectedCart() should return the selected Entity', () => {
      const result = cartQuery.getSelectedCart(storeState);
      const selId = getCartId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it("getLoaded() should return the current 'loaded' status", () => {
      const result = cartQuery.getLoaded(storeState);

      expect(result).toBe(true);
    });

    it("getError() should return the current 'error' storeState", () => {
      const result = cartQuery.getError(storeState);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
