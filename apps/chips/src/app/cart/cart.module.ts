import {
  CART_FEATURE_KEY,
  initialState as cartInitialState,
  cartReducer
} from './+state/cart.reducer';
import { CartEffects } from './+state/cart.effects';
import { CartFacade } from './+state/cart.facade';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart/cart.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CartBackendService } from './services/cart-backend.service';
import { CartResolverService } from './services/cart-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    CartRoutingModule,
    StoreModule.forFeature(CART_FEATURE_KEY, cartReducer, {
      initialState: cartInitialState
    }),
    EffectsModule.forFeature([CartEffects])
  ],
  declarations: [CartComponent],
  providers: [CartFacade, CartResolverService, CartBackendService]
})
export class CartModule {}
