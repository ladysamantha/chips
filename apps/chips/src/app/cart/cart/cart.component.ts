import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';
import { CartState } from '../+state/cart.reducer';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'chips-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cart$: Observable<CartState>;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.cart$ = this.route.data.pipe(
      map((data: { cart: CartState }) => data.cart)
    );
  }
}
