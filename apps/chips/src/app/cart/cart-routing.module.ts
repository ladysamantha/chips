import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { CartResolverService } from './services/cart-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: CartComponent,
    resolve: {
      cart: CartResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartRoutingModule {}
