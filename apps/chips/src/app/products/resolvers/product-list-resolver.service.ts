import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tap, first } from 'rxjs/operators';
import { ProductsFacade } from '../+state/products.facade';

@Injectable()
export class ProductListResolverService implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.productsFacade.allProducts$.pipe(
      first(),
    );
  }

  constructor(private productsFacade: ProductsFacade) {
    productsFacade.loadAll();
  }
}
