import { TestBed } from '@angular/core/testing';

import { ProductListResolverService } from './product-list-resolver.service';
import { ProductsFacade, IProductsFacade } from '../+state/products.facade';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Product } from '../+state/products.reducer';
import { readFirst } from '@nrwl/nx/testing';

@Injectable()
class MockProductsFacade implements IProductsFacade {
  loaded$: Observable<boolean>;
  allProducts$: Observable<Product[]> = of([]);
  selectedProducts$: Observable<Product>;
  loadAll = jest.fn();
}

describe('ProductListResolverService', () => {
  let facade: IProductsFacade;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProductListResolverService,
        {
          provide: ProductsFacade,
          useClass: MockProductsFacade
        }
      ]
    });
    facade = TestBed.get(ProductsFacade);
  });

  it('should be created', () => {
    const service: ProductListResolverService = TestBed.get(
      ProductListResolverService
    );
    expect(service).toBeTruthy();
  });

  it('should call facade.loadAll', async () => {
    const service: ProductListResolverService = TestBed.get(
      ProductListResolverService
    );
    const products = await readFirst(service.resolve(null, null));
    expect(facade.loadAll).toBeCalledTimes(1);
    expect(products).toEqual([]);
  });
});
