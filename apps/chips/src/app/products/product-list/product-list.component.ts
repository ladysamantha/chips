import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../+state/products.reducer';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CartFacade } from '../../cart/+state/cart.facade';

@Component({
  selector: 'chips-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductListComponent implements OnInit {
  products$: Observable<Product[]>;

  constructor(private route: ActivatedRoute, private cartFacade: CartFacade) {}

  ngOnInit() {
    this.products$ = this.route.data.pipe(
      map((data: { products: Product[] }) => data.products)
    );
  }

  addProductToCart(product: Product) {
    this.cartFacade.addProductToCart(product);
  }
}
