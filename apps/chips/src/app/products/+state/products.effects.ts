import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import * as cuid from 'cuid';

import { ProductsPartialState } from './products.reducer';
import {
  LoadProducts,
  ProductsLoaded,
  ProductsLoadError,
  ProductsActionTypes
} from './products.actions';
import { ProductBackendService } from '../services/product-backend.service';
import { map } from 'rxjs/operators';

@Injectable()
export class ProductsEffects {
  @Effect() loadProducts$ = this.dataPersistence.fetch(
    ProductsActionTypes.LoadProducts,
    {
      run: (action: LoadProducts, state: ProductsPartialState) =>
        this.backend
          .getAllProducts()
          .pipe(map(products => new ProductsLoaded(products))),

      onError: (action: LoadProducts, error) => {
        console.error('Error', error);
        return new ProductsLoadError(error);
      }
    }
  );

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<ProductsPartialState>,
    private backend: ProductBackendService
  ) {}
}
