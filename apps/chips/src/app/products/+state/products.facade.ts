import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { ProductsPartialState, Product } from './products.reducer';
import { productsQuery } from './products.selectors';
import { LoadProducts } from './products.actions';

export interface IProductsFacade {
  readonly loaded$: Observable<boolean>;
  readonly allProducts$: Observable<Product[]>;
  readonly selectedProducts$: Observable<Product>;

  loadAll(): void;
}

@Injectable()
export class ProductsFacade implements IProductsFacade {
  readonly loaded$ = this.store.pipe(select(productsQuery.getLoaded));
  readonly allProducts$ = this.store.pipe(select(productsQuery.getAllProducts));
  readonly selectedProducts$ = this.store.pipe(
    select(productsQuery.getSelectedProducts)
  );

  constructor(private store: Store<ProductsPartialState>) {}

  loadAll() {
    this.store.dispatch(new LoadProducts());
  }
}
