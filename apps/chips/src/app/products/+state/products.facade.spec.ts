import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { readFirst } from '@nrwl/nx/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/nx';

import { ProductsEffects } from './products.effects';
import { ProductsFacade } from './products.facade';

import { productsQuery } from './products.selectors';
import { LoadProducts, ProductsLoaded } from './products.actions';
import {
  ProductsState,
  Product,
  initialState,
  productsReducer
} from './products.reducer';

interface TestSchema {
  products: ProductsState;
}

describe('ProductsFacade', () => {
  let facade: ProductsFacade;
  let store: Store<TestSchema>;
  let createProducts;

  beforeEach(() => {
    createProducts = (id: string, name = ''): Product => ({
      id,
      name: name || `name-${id}`,
      price: 3.0
    });
  });

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature('products', productsReducer, { initialState }),
          EffectsModule.forFeature([ProductsEffects])
        ],
        providers: [ProductsFacade]
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule
        ]
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.get(Store);
      facade = TestBed.get(ProductsFacade);
    });

    it('loadAll() should return list with loaded == true', async done => {
      try {
        let list = await readFirst(facade.allProducts$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.loadAll();

        list = await readFirst(facade.allProducts$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(1);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `ProductsLoaded` to manually submit list for state management
     */
    it('allProducts$ should return the loaded list; and loaded flag == true', async done => {
      try {
        let list = await readFirst(facade.allProducts$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        store.dispatch(
          new ProductsLoaded([createProducts('AAA'), createProducts('BBB')])
        );

        list = await readFirst(facade.allProducts$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(2);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });
});
