import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output
} from '@angular/core';
import { Product } from '../+state/products.reducer';

@Component({
  selector: 'chips-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailComponent {
  @Input() product: Product;
  @Output() addToProductClicked = new EventEmitter<Product>();

  addProductToCart() {
    this.addToProductClicked.emit(this.product);
  }
}
