import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  PRODUCTS_FEATURE_KEY,
  initialState as productsInitialState,
  productsReducer
} from './+state/products.reducer';
import { ProductsEffects } from './+state/products.effects';
import { ProductsFacade } from './+state/products.facade';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductListResolverService } from './resolvers/product-list-resolver.service';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductBackendService } from './services/product-backend.service';
import { CartModule } from '../cart/cart.module';

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    CartModule,
    StoreModule.forFeature(PRODUCTS_FEATURE_KEY, productsReducer, {
      initialState: productsInitialState
    }),
    EffectsModule.forFeature([ProductsEffects])
  ],
  declarations: [ProductListComponent, ProductDetailComponent],
  providers: [ProductsFacade, ProductListResolverService, ProductBackendService]
})
export class ProductsModule {}
