import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Product } from '../+state/products.reducer';
import cuid = require('cuid');

@Injectable()
export class ProductBackendService {
  constructor() {}

  getAllProducts(): Observable<Product[]> {
    return of([
      {
        id: cuid(),
        name: 'Sweet Sugar Cookie',
        price: 3.0
      }
    ]);
  }
}
