import { Component } from '@angular/core';

@Component({
  selector: 'chips-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'chips';
}
